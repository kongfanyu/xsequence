package com.xuan.xseq;

import com.alibaba.druid.pool.DruidDataSource;
import com.xuan.xseq.range.impl.db.DbSeqRangeConfig;
import com.xuan.xseq.range.impl.db.DbSeqRangeMgr;
import com.xuan.xseq.seq.Sequence;
import com.xuan.xseq.seq.impl.DefaultSequence;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by xuan on 2018/1/10.
 */
public class SequenceTest_Api {

    private Sequence userSeq;

    @Before
    public void setup() {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setUrl("jdbc:mysql://121.196.218.206:3306/admin?characterEncoding=UTF-8");
        dataSource.setUsername("admin");
        dataSource.setPassword("admin123");
        dataSource.setMaxActive(300);
        dataSource.setMinIdle(50);
        dataSource.setInitialSize(2);
        dataSource.setMaxWait(500);

        DbSeqRangeConfig seqRangeConfig = new DbSeqRangeConfig();
        seqRangeConfig.setDataSource(dataSource);
        seqRangeConfig.setTableName("sequence");
        seqRangeConfig.setRetryTimes(100);
        seqRangeConfig.setStep(1000);
        seqRangeConfig.setStepStart(0);

        DbSeqRangeMgr seqRangeMgr = new DbSeqRangeMgr();
        seqRangeMgr.setSeqRangeConfig(seqRangeConfig);
        seqRangeMgr.init();

        userSeq = new DefaultSequence();
        userSeq.setName("user");
        userSeq.setSeqRangeMgr(seqRangeMgr);
    }

    @Test
    public void test() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 1000; i++) {
            System.out.println("++++++++++id:" + userSeq.nextValue());
        }
        System.out.println("interval time:" + (System.currentTimeMillis() - start));
    }

}
