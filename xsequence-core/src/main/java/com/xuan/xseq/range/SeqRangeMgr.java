package com.xuan.xseq.range;

import com.xuan.xseq.exception.SequenceException;

/**
 * 区间管理器
 * Created by xuan on 2018/1/10.
 */
public interface SeqRangeMgr {

    /**
     * 获取指定区间名的下一个区间
     *
     * @param name 区间名
     * @return 返回区间
     * @throws SequenceException 异常
     */
    SeqRange nextRange(String name) throws SequenceException;

    /**
     * 设置配置参数
     *
     * @param seqRangeConfig 配置参数
     */
    void setSeqRangeConfig(AbstractSeqRangeConfig seqRangeConfig);

    /**
     * 初始化
     */
    void init();
}
